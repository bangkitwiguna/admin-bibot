<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use DB;

class dataExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $view;
    public $data;

    public function __construct($view, $data = "")
    {
        $this->view = $view;
        $this->data = $data;
    }

    public function view():View
    {
        // return DB::table('produk')->where('produk.status', 1)
        // ->leftJoin('users', DB::RAW("CAST(produk.updated_by as INT)"), '=', 'users.id')
        // ->leftJoin ('produk_kombinasi_detail', 'produk.kd_produk', '=', 'produk_kombinasi_detail.kd_produk')
        // ->select('produk.kd_produk', DB::RAW("CAST(produk_kombinasi_detail.sku as VARCHAR)"), 'produk.nama_produk', 'produk_kombinasi_detail.stock', 'produk_kombinasi_detail.booking', 'produk.updated_at', 'users.name')        
        // ->orderBy('produk.updated_at', 'desc')
        // ->get(); 
        
        return view(
            $this->view, ['isi'=>$this->data]
        );

    }
}
