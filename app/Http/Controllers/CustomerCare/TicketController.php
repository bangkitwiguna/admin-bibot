<?php

namespace App\Http\Controllers\CustomerCare;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApihandlerController as FETCH;
use Illuminate\Support\Arr;
use Illuminate\Routing\Redirector;

use App\Exports\dataExport;
use Maatwebsite\Excel\Facades\Excel;

class TicketController extends Controller
{
  public function __construct(Redirector $redirect)
  {
    $this->data = [
        'url'=>'ticket',
        'title'=>'E-Ticket',
        'tableHeader'=>['No', 'Ticket No', 'Username', 'Subject', 'Message', 'Date', 'Detail'],
    ];
    $this->data=(object) $this->data;
  }

    public function index()
    {
        if (session("token") == "" || session("token") == null){
            return redirect('/login');
        }   

        return view('layout-list', ['data'=> $this->data]);
    }

  public function store(Request $request)
  {
      $ke = $request->ke;
      if($ke == 'editData'){ return $this->editData($request); }
      if($ke == 'updateData'){ return $this->updateData($request); }
      if($ke == 'deleteData'){ return $this->deleteData($request); }
      if($ke == 'dataAll'){ return $this->dataAll($request); }
  }

  public function deleteData($request)
  {
    $url = env('API').'member/'.$request->dataCode;
    $data = FETCH::API($url, [], 'DELETE', session("token"));
    if($data->status == "success"){
      return ['rtn'=>1, 'msg'=>"Success"];
    } else {
      return ['rtn'=>0, 'msg'=>ucfirst($data->message)];
    }
  }

  public function editData($request)
  {
    $kirim = [
      'kd_ticket' => $request->dataCode
    ];
    
    $url = env('API').'det-ticket';
    $data = FETCH::API($url, $kirim, 'POST', session("token"));
    return view('customer_care.ticket.form', ['data'=> $this->data, 'ke'=>'updateData', 'disab'=>'', 'isi'=>$data->data]);
  }
  
  public function updateData($request)
  {
    if($request->message == ""){
      return ['rtn'=>0, 'msg'=>"Message can't be Empty"];
    }
    $kirim = [
      'description' => $request->message,
      'kd_ticket' => $request->kd_ticket
    ];
    
    $url = env('API').'reply-ticket';
    $data = FETCH::API($url, $kirim, 'POST', session("token"));

    if($data->status == "success"){
      return ['rtn'=>1, 'msg'=>"Success"];
    } else {
      return ['rtn'=>0, 'msg'=>ucfirst($data->message)];
    }
  }

  public function show($id)
  {
      if($id == 'backHome'){ return $this->backHome(); }
      if($id == 'getForm'){ return $this->getForm(); }
  }

  public function backHome(){
      return view('layout-table', ['data'=> $this->data, 'show_add'=>0]);
  }

  public function dataAll($request)
  {
    $url = env('API').'admin/ticket';

    $data = FETCH::API($url, [], 'POST', session("token"));
    $table = '';
    $i = 0;
    foreach($data->data as $dta => $dt){
        $i++;
        $detail = '<i class="fa fa-eye pointer text-primary edit" data-code="'.$dt->kd_ticket.'" ></i>';

        if ($dt->user != 'Mzone'){
          $message = '<b>'.$dt->message.'</b>';
        } else {
          $message = $dt->message;
        }

        $table = $table.'<tr>
            <td>'.$i.'</td>
            <td>'.$dt->kd_ticket.'</td>
            <td>'.$dt->username.'</td>
            <td>'.$dt->subject.'</td>
            <td>'.$message.'</td>
            <td>'.$dt->date.'</td>
            <td>'.$detail.'</td>
        </tr>';
        // <td>'.$dt->status.'</td>
    }

    return ['table' => $table];
  }

  public function genKode()
  {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return date('ymd').$randomString.date('his').rand(1,99);
  }

  public function cekValidasi($request)
  {
      $pesan = [
          'nama_outlet.required' => 'Nama Outlet tidak boleh kosong',
      ];

      return $validator =  $this->validate($request, [
          'nama_outlet' => 'required',
      ],$pesan);
  }



}
