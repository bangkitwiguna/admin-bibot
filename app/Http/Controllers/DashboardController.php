<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApihandlerController as FETCH;
use Illuminate\Support\Arr;
use Illuminate\Routing\Redirector;

class DashboardController extends Controller
{
  public function __construct(Redirector $redirect)
  {
    $this->data = [
      'url'=>'dashboard',
    ];
    $this->data=(object) $this->data;
  }

  public function index()
  {
    if(session('fa')==true && session("token") != null){
      return redirect('/googleauth');
    }
    else if (session("token") == "" || session("token") == null){
      return redirect('/login');
    }
    
    // $tradeAccount = FETCH::API(env('API').'trade-accounts', [], 'GET', session("token"));
    // $dashboard = FETCH::API(env('API3').'withdraw/dashboard', [], 'GET', session("token"));
    // $binance = FETCH::API(env('API3').'withdraw/balance', [], 'GET', session("token"));
    // $dashboard2 = FETCH::API(env('API').'dashboard', [], 'GET', session("token"));
    
    $lang = session('lang');
    $lg = '';
    if ($lang == 'id') {
        $lg = 'id.';
    }

    // $super = FETCH::API(env('API').'users/'.session('id'), [], 'GET', session("token"));
    // session(['super_admin' => $super->data->super_admin]);

    return view(
      $lg.'dashboard.dashboard',
      [
        'data'=> $this->data,
        // 'dashboard'=>$dashboard->data,
        // 'binance'=>$binance->data[0],
        // 'dashboard2'=>$dashboard2->data,
      ]
    );
  }

  public function bahasa(Request $request)
  {
      session(['lang' => $request->lag]);
      return redirect()->back();
  }
}