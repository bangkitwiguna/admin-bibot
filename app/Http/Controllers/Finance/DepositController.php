<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApihandlerController as FETCH;
use Illuminate\Support\Arr;
use Illuminate\Routing\Redirector;

use App\Exports\dataExport;
use Maatwebsite\Excel\Facades\Excel;

class DepositController extends Controller
{
  public function __construct(Redirector $redirect)
  {
    $this->data = [
        'url'=>'deposit',
        'title'=>'Master Deposit',
        'tableHeader'=>['No', 'Deposit No', 'Username', 'Address', 'Network', 'Amount', 'Date', 'Status'],
    ];
    $this->data=(object) $this->data;
  }

    public function index()
    {
        if (session("token") == "" || session("token") == null){
            return redirect('/login');
        }   

        return view('layout-list', ['data'=> $this->data]);
    }

  public function store(Request $request)
  {
      $ke = $request->ke;
      if($ke == 'editData'){ return $this->editData($request); }
      if($ke == 'updateData'){ return $this->updateData($request); }
      if($ke == 'deleteData'){ return $this->deleteData($request); }
      if($ke == 'dataAll'){ return $this->dataAll($request); }
  }

  public function deleteData($request)
  {
    $url = env('API').'member/'.$request->dataCode;
    $data = FETCH::API($url, [], 'DELETE', session("token"));
    if($data->status == "success"){
      return ['rtn'=>1, 'msg'=>"Success"];
    } else {
      return ['rtn'=>0, 'msg'=>ucfirst($data->message)];
    }
  }

  public function editData($request)
  {
    $data = FETCH::API(env('API').'members/'.$request->dataCode, [], 'GET', session("token"));
    $robotMember = FETCH::API(env('API').'robot/user/'.$request->dataCode, [], 'GET', session("token"));
    $role = FETCH::API(env('API').'users/'.session('id'), [], 'GET', session("token"));
    return view('member_data.master_member.form', ['data'=> $this->data, 'disab'=>'readonly', 'ke'=>'updateData', 'isi'=>$data->data, 'robotMember'=>$robotMember->data, 'role'=>$role->data]);
  }

  public function show($id)
  {
      if($id == 'backHome'){ return $this->backHome(); }
      if($id == 'getForm'){ return $this->getForm(); }
  }

  public function backHome(){
      return view('layout-table', ['data'=> $this->data, 'filter'=>'financial_management.deposit_history.filter', 'show_add'=>0]);
  }

  public function dataAll($request)
  {
    $url = env('API').'admin/deposit';

    $kirim = [
      'limit' => $request->pageLoaded,
      'page' => $request->currentPage,
      'status' => $request->status,
      'startDate' => $request->start_date,
      'endDate' => $request->end_date,
    ];

    $data = FETCH::API($url, $kirim, 'POST', session("token"));
    $table = '';
    $i = 0;
    foreach($data->data->dp as $dta => $dt){
        $i++;
        // $detail = '<i class="fa fa-eye pointer text-primary edit" data-code="'.$dt->username.'" ></i>';
        // $delete = '<i class="fa fa-trash pointer text-danger delete" data-code="'.$dt->username.'" ></i>';
        // $rankdetail = '<i class="fas fa-network-wired pointer text-warning rankDetail" data-code="'.$dt->kd_member.'" ></i>';
        $table = $table.'<tr>
            <td>'.$i.'</td>
            <td>'.$dt->depo_no.'</td>
            <td>'.$dt->username.'</td>
            <td>'.$dt->address.'</td>
            <td>'.$dt->network.'</td>
            <td>'.$dt->amount.'</td>
            <td>'.$dt->created_at.'</td>
            <td>'.$dt->status.'</td>
        </tr>';
    }

    return ['table' => $table, 'pagination' => $data->data->pagination];
  }

  public function genKode()
  {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return date('ymd').$randomString.date('his').rand(1,99);
  }

  public function cekValidasi($request)
  {
      $pesan = [
          'nama_outlet.required' => 'Nama Outlet tidak boleh kosong',
      ];

      return $validator =  $this->validate($request, [
          'nama_outlet' => 'required',
      ],$pesan);
  }



}
