<?php

namespace App\Http\Controllers\Finance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApihandlerController as FETCH;
use Illuminate\Support\Arr;
use Illuminate\Routing\Redirector;

use App\Exports\dataExport;
use Maatwebsite\Excel\Facades\Excel;

class WithdrawController extends Controller
{
  public function __construct(Redirector $redirect)
  {
    $this->data = [
        'url'=>'withdraw',
        'title'=>'Master Withdraw',
        'tableHeader'=>['No', 'Withdraw No', 'Username', 'Network', 'Gross', 'Fee', 'Actual Amount', 'Type', 'Date', 'Status', 'Address'],
    ];
    $this->data=(object) $this->data;
  }

    public function index()
    {
        if (session("token") == "" || session("token") == null){
            return redirect('/login');
        }   

        return view('layout-list', ['data'=> $this->data]);
    }

  public function store(Request $request)
  {
      $ke = $request->ke;
      if($ke == 'editData'){ return $this->editData($request); }
      if($ke == 'updateData'){ return $this->updateData($request); }
      if($ke == 'deleteData'){ return $this->deleteData($request); }
      if($ke == 'dataAll'){ return $this->dataAll($request); }
  }

  public function deleteData($request)
  {
    $url = env('API').'member/'.$request->dataCode;
    $data = FETCH::API($url, [], 'DELETE', session("token"));
    if($data->status == "success"){
      return ['rtn'=>1, 'msg'=>"Success"];
    } else {
      return ['rtn'=>0, 'msg'=>ucfirst($data->message)];
    }
  }

  public function editData($request)
  {
    $data = FETCH::API(env('API').'members/'.$request->dataCode, [], 'GET', session("token"));
    $robotMember = FETCH::API(env('API').'robot/user/'.$request->dataCode, [], 'GET', session("token"));
    $role = FETCH::API(env('API').'users/'.session('id'), [], 'GET', session("token"));
    return view('member_data.master_member.form', ['data'=> $this->data, 'disab'=>'readonly', 'ke'=>'updateData', 'isi'=>$data->data, 'robotMember'=>$robotMember->data, 'role'=>$role->data]);
  }

  public function show($id)
  {
      if($id == 'backHome'){ return $this->backHome(); }
      if($id == 'getForm'){ return $this->getForm(); }
  }

  public function backHome(){
      return view('layout-table', ['data'=> $this->data, 'filter'=>'financial_management.withdraw_history.filter', 'show_add'=>0]);
  }

  public function dataAll($request)
  {
    $url = env('API').'admin/withdraw';

    $kirim = [
      'limit' => $request->pageLoaded,
      'page' => $request->currentPage,
      'status' => $request->status,
      'startDate' => $request->start_date,
      'endDate' => $request->end_date,
    ];

    $data = FETCH::API($url, $kirim, 'POST', session("token"));
    $table = '';
    $i = 0;
    foreach($data->data->wd as $dta => $dt){
        $i++;

        $table = $table.'<tr>
            <td>'.$i.'</td>
            <td>'.$dt->wd_no.'</td>
            <td>'.$dt->username.'</td>
            <td>'.$dt->network.'</td>
            <td>'.$dt->gross_amount.'</td>
            <td>'.$dt->fee_amount.'</td>
            <td>'.$dt->actual_amount.'</td>
            <td>'.$dt->wd_type.'</td>
            <td>'.$dt->created_at.'</td>
            <td>'.$dt->status.'</td>
            <td>'.$dt->address.'</td>
        </tr>';
    }

    return ['table' => $table, 'pagination' => $data->data->pagination];
  }

  public function genKode()
  {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return date('ymd').$randomString.date('his').rand(1,99);
  }

  public function cekValidasi($request)
  {
      $pesan = [
          'nama_outlet.required' => 'Nama Outlet tidak boleh kosong',
      ];

      return $validator =  $this->validate($request, [
          'nama_outlet' => 'required',
      ],$pesan);
  }



}
