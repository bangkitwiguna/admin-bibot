<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApihandlerController as FETCH;
use Illuminate\Routing\Redirector;

// use Session;

class LoginController extends Controller

{
  public function __construct(Redirector $redirect)
  {
      $this->data = [
          'url'=>'login',
      ];
      $this->data=(object) $this->data;
  }

  public function index()
  {
    return view(
      'home/login',
      [
        'data'=> $this->data,
      ]
    );
  }

  public function store(Request $request)
  {
    $ke = $request->ke;
    if($ke == 'login'){ return $this->login($request); }
  }

	public function login(Request $request)
	{   
    $fields = array(
        'username' => $request->username,
        'password' => $request->password,
    );

    $api_url = env('API').'login';
    $headers = array('Content-Type: application/x-www-form-urlencoded');
    $ch = curl_init($api_url);
    $postvars = http_build_query($fields);

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $postvars);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    $res1 = json_decode($response,true);                   
    if($res1['status'] == 'success'){
      session([
        'token' => $res1['data']['token'],
        'name' => $res1['data']['name'],
      ]);
      return ['rtn'=>1, 'info'=>$res1];
    }else{            
      $err = '';
      if(is_string($res1['message'])){
        $err = $err.ucfirst(strtolower($res1['message']));
      }else{
        foreach($res1['message'] as $msg => $ms){
          $rp1 = str_replace('_', ' ', $ms['message']);
          $ms = preg_replace('/[^A-Za-z0-9\- ]/', '', $rp1);
          $err = $err.ucfirst(strtolower($ms));
        }                
      }

      return ['rtn'=>0, 'msg'=>$err, 'info'=>$res1];
    }
  }
}