<?php

namespace App\Http\Controllers\MemberData;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApihandlerController as FETCH;
use Illuminate\Support\Arr;
use Illuminate\Routing\Redirector;

use App\Exports\dataExport;
use Maatwebsite\Excel\Facades\Excel;

class MemberController extends Controller
{
  public function __construct(Redirector $redirect)
  {
    $this->data = [
        'url'=>'master-member',
        'title'=>'Master Member',
        'tableHeader'=>['No', 'Full Name', 'Username', 'Referral', 'Email', 'Country', 'Status', 'Teams', 'Detail'],
    ];
    $this->data=(object) $this->data;
  }

    public function index()
    {
        if (session("token") == "" || session("token") == null){
            return redirect('/login');
        }   

        return view('layout-list', ['data'=> $this->data]);
    }

  public function show($id)
  {
      if($id == 'backHome'){ return $this->backHome(); }
  }

  public function backHome(){
      return view('layout-table', ['data'=> $this->data, 'filter'=>'member_data.master_member.filter', 'show_add'=>0]);
  }

  public function store(Request $request)
  {
      $ke = $request->ke;
      if($ke == 'editData'){ return $this->editData($request); }
      if($ke == 'dataAll'){ return $this->dataAll($request); }
      if($ke == 'rankDetail'){ return $this->rankDetail($request); }
  }

  public function editData($request)
  {
    $kirim = [
      'kd_user' => $request->dataCode
    ];
    $data = FETCH::API(env('API2').'member-detail/', $kirim, 'POST', session("token"));
    return view('member_data.master_member.form', ['data'=> $this->data, 'disab'=>'readonly', 'ke'=>'updateData', 'isi'=>$data->data->information, 'wallet'=>$data->data->wallet]);
  }

  public function dataAll($request)
  {
    $url = env('API').'admin/member';

    $kirim = [
      'limit' => $request->pageLoaded,
      'page' => $request->currentPage,
      'status' => $request->status,
      'username' => $request->username,
    ];

    $data = FETCH::API($url, $kirim, 'POST', session("token"));
    $table = '';
    $i = 0;
    foreach($data->data->user as $dta => $dt){
        $i++;
        $detail = '<i class="fa fa-eye pointer text-primary edit" data-code="'.$dt->kd_user.'" ></i>';
        $teams = '<i class="fas fa-network-wired pointer text-warning rankDetail" data-code="'.$dt->kd_user.'" ></i>';
        $status = ['Non Active', 'Active'];
        $table = $table.'<tr>
            <td>'.$i.'</td>
            <td>'.$dt->username.'</td>
            <td>'.$dt->name.'</td>
            <td>'.$dt->referall.'</td>
            <td>'.$dt->email.'</td>
            <td>'.$dt->country.'</td>
            <td>'.$status[$dt->status].'</td>
            <td>'.$teams.'</td>
            <td>'.$detail.'</td>
        </tr>';
    }

    return ['table' => $table, 'pagination' => $data->data->pagination];
  }

  public function rankDetail($request)
  {
    $url = env('API2').'member-referral';
    $kirim = [
      'kd_user' => $request->dataCode
    ];
    $data = FETCH::API($url, $kirim, 'POST', session("token"));
    return view('member_data.master_member.form-rank', ['table' => $data->data]);
  }

  public function genKode()
  {
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < 5; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return date('ymd').$randomString.date('his').rand(1,99);
  }

  public function cekValidasi($request)
  {
      $pesan = [
          'nama_outlet.required' => 'Nama Outlet tidak boleh kosong',
      ];

      return $validator =  $this->validate($request, [
          'nama_outlet' => 'required',
      ],$pesan);
  }



}
