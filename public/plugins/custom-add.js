function simpan(idform, btnsubmit, url, redEdit=0, modalid=null, refresh=0){
    console.log(JSON.stringify(new FormData($("#"+idform)[0])))
    
  $('#'+btnsubmit).attr('disabled', 'disabled');
  $('#'+btnsubmit).html('loading ...');
  $('#modalLoading').modal("show");
  $('.text-danger').html('');
console.log(idform)
  console.log('Simpans');
  $.ajax({
    url: url,
    type: 'POST',
    data:new FormData($("#"+idform)[0]),
    contentType: false, 
    processData: false,

    success: function(data) {
      console.log(data);
      if(data.rtn == 1){
        setTimeout(function(){
          var err = '<div id="alert-success" class="alert alert-success" role="alert">'+data.status+'</div>';
          $('#alert-home').html(err);
          $('#alert-homes').html(err);
          hideSlow('alert-success');
        }, 1100);
        toast(data.msg, data.rtn);

        setTimeout(function(){
          location.reload(true)
        }, 900);
      }

      if(data.rtn == 0){
        console.log(data.msg)
        var err = ''
        $.map(data.msg, function( val, i ) {
          toast(val.message, data.rtn);
          err = err+'<div id="alert-failed" class="alert alert-danger" role="alert">'+val.message+'</div>';
        });
        console.log(err)

        $('#alert-home').html(err);
        $('#alert-homes').html(err);
        hideSlow('alert-failed');
      }

      $('#'+btnsubmit).prop('disabled', false);
      $('#'+btnsubmit).html('Simpan');

      if(modalid != null){
        $('#'+modalid).modal('hide');
      }

      $('#modalLoading').modal('hide');
    },
    error :function( data ) {
      console.log(data.responseText);
      toastr.clear();
      var errors = $.parseJSON(data.responseText);
      $.each(errors.errors, function (key, value) {
        $('span#'+key+'_error').html(value[0]);

        var sts = value[0];
        toast(sts, 0);
      });
      $('#'+btnsubmit).prop('disabled', false);
      $('#'+btnsubmit).html('Simpan');
      $('#modalLoading').modal('hide');
    }
  });
}

function approvedData(btnapproved, btnsubmit, url, token, dataCode){
  $('#'+btnsubmit).attr('disabled', 'disabled');
  $('#'+btnsubmit).html('loading ...');
  $('#'+btnapproved).attr('disabled', 'disabled');
  $('#'+btnapproved).html('loading ...');
  $('.text-danger').html('');

  $('#modalLoading').modal("show");

  // var dataCode = $('#kode').val();
  // console.log(dataCode)
  $.ajax({
    type: "POST",
    data: {_token :token, ke:'approvedData', dataCode : dataCode },
    url: url,
    success: function (data) {
      // console.log(data);

      if(data.rtn == 1){
        backHome();
        setTimeout(function(){
          var err = '<div id="alert-success" class="alert alert-success" role="alert">'+data.status+'</div>';
          $('#alert-home').html(err);
          $('#alert-homes').html(err);
          hideSlow('alert-success');
        }, 900);
      }
      if(data.rtn == 0){
        var err = '<div id="alert-failed" class="alert alert-danger" role="alert">'+data.status+'</div>';
        $('#alert-home').html(err);
        $('#alert-homes').html(err);
        setTimeout(function(){
          hideSlow('alert-failed');
        }, 2000);
      }
      $('#'+btnsubmit).prop('disabled', false);
      $('#'+btnsubmit).html('Simpan');

      $('#'+btnapproved).prop('disabled', false);
      $('#'+btnapproved).html('Setujui Data');

      $('#modalLoading').modal('hide');
      toast(data.status, data.rtn);

      setTimeout(function(){ location.reload(true); }, 4000);

    },
    error :function( data ) {
      console.log(data.responseText);

      var sts = 'Failed to Simpan! Check error';
      toast(sts, 0);
    }
  });
}



function reopenData(btnapproved, btnsubmit, url, token, dataCode){
  $('#'+btnsubmit).attr('disabled', 'disabled');
  $('#'+btnsubmit).html('loading ...');
  $('#'+btnapproved).attr('disabled', 'disabled');
  $('#'+btnapproved).html('loading ...');
  $('.text-danger').html('');

  $('#modalLoading').modal("show");

  // var dataCode = $('#kode').val();
  // console.log(dataCode)
  $.ajax({
    type: "POST",
    data: {_token :token, ke:'reopenData', dataCode : dataCode },
    url: url,
    success: function (data) {
      // console.log(data);

      if(data.rtn == 1){
        backHome();
        setTimeout(function(){
          var err = '<div id="alert-success" class="alert alert-success" role="alert">'+data.status+'</div>';
          $('#alert-home').html(err);
          $('#alert-homes').html(err);
          hideSlow('alert-success');
        }, 900);
      }
      if(data.rtn == 0){
        var err = '<div id="alert-failed" class="alert alert-danger" role="alert">'+data.status+'</div>';
        $('#alert-home').html(err);
        $('#alert-homes').html(err);
        setTimeout(function(){
          hideSlow('alert-failed');
        }, 2000);
      }
      $('#'+btnsubmit).prop('disabled', false);
      $('#'+btnsubmit).html('Simpan');

      $('#'+btnapproved).prop('disabled', false);
      $('#'+btnapproved).html('Setujui Data');

      $('#modalLoading').modal('hide');
      toast(data.status, data.rtn);

      setTimeout(function(){ location.reload(true); }, 4000);

    },
    error :function( data ) {
      console.log(data.responseText);

      var sts = 'Failed to Simpan! Check error';
      toast(sts, 0);
    }
  });
}



function approvedData(btnapproved, btnsubmit, url, token, dataCode){
  $('#'+btnsubmit).attr('disabled', 'disabled');
  $('#'+btnsubmit).html('loading ...');
  $('#'+btnapproved).attr('disabled', 'disabled');
  $('#'+btnapproved).html('loading ...');
  $('.text-danger').html('');

  $('#modalLoading').modal("show");

  // var dataCode = $('#kode').val();
  // console.log(dataCode)
  $.ajax({
    type: "POST",
    data: {_token :token, ke:'approvedData', dataCode : dataCode },
    url: url,
    success: function (data) {
      // console.log(data);

      if(data.rtn == 1){
        backHome();
        setTimeout(function(){
          var err = '<div id="alert-success" class="alert alert-success" role="alert">'+data.status+'</div>';
          $('#alert-home').html(err);
          $('#alert-homes').html(err);
          hideSlow('alert-success');
        }, 900);
      }
      if(data.rtn == 0){
        var err = '<div id="alert-failed" class="alert alert-danger" role="alert">'+data.status+'</div>';
        $('#alert-home').html(err);
        $('#alert-homes').html(err);
        setTimeout(function(){
          hideSlow('alert-failed');
        }, 2000);
      }
      $('#'+btnsubmit).prop('disabled', false);
      $('#'+btnsubmit).html('Simpan');

      $('#'+btnapproved).prop('disabled', false);
      $('#'+btnapproved).html('Setujui Data');

      $('#modalLoading').modal('hide');
      toast(data.status, data.rtn);

      setTimeout(function(){ location.reload(true); }, 4000);

    },
    error :function( data ) {
      console.log(data.responseText);

      var sts = 'Failed to Simpan! Check error';
      toast(sts, 0);
    }
  });
}




function simpanProses(btnProses, url, token, ke){
  $('#'+btnProses).attr('disabled', 'disabled');
  $('#'+btnProses).html('loading ...');
  $('.text-danger').html('');

  $('#modalLoading').modal("show");

  var dataCode = $('#id').val();
  $.ajax({
    type: "POST",
    data: {_token :token, ke:ke, dataCode : dataCode },
    url: url,
    success: function (data) {
      console.log(data);

      if(data.rtn == 1){
        backHome();
        setTimeout(function(){
          var err = '<div id="alert-success" class="alert alert-success" role="alert">'+data.status+'</div>';
          $('#alert-home').html(err);
          $('#alert-homes').html(err);
          hideSlow('alert-success');
        }, 900);
      }
      if(data.rtn == 0){
        var err = '<div id="alert-failed" class="alert alert-danger" role="alert">'+data.status+'</div>';
        $('#alert-home').html(err);
        $('#alert-homes').html(err);
        hideSlow('alert-failed');
      }
      $('#'+btnProses).prop('disabled', false);
      $('#'+btnProses).html('Simpan');
      $('#modalLoading').modal('hide');
      toast(data.status, data.rtn);
    },
    error :function( data ) {
      console.log(data.responseText);

      var sts = 'Failed to Simpan! Check error';
      toast(sts, 0);
    }
  });
}


function select2(){
  $('.select2').select2({
    theme: 'bootstrap4',
    width: '100%'
  });
}

function hideSlow(divId){
  setTimeout(function(){
    $('#'+divId).hide(2000);
  }, 3000);
}


function formatAngka() {
  $(document).ready(function() {
    $(".format-angka").change(function() {
      var angk = $(this).val().replace(/[^\d.-]/g, '');
      var hslBil = formatMoney(angk, 2)
      $(this).val(hslBil);
    });


    $(".format-int").change(function() {
      var angk = $(this).val().replace(/[^\d.-]/g, '');
      var hslBil = formatMoney(angk, 0)
      $(this).val(hslBil);
    });

    $(".format-des").change(function() {
      var angk = $(this).val().replace(/[^\d.-]/g, '');
      var hslBil = formatMoney(angk, 3)
      $(this).val(hslBil);
    });

  });
}


function cleanAngka(angk) {
  return angk.replace(/[^\d.-]/g, '');
}

function buatAngka(angk, koma=0) {
  var hslBil = formatMoney(angk, koma)
  return hslBil;
}


function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

    const negativeSign = amount < 0 ? "-" : "";

    let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    let j = (i.length > 3) ? i.length % 3 : 0;

    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
};



function selectFocus() {
  $(document).ready(function() {
    $(".focus").click(function() {
      if(cleanAngka($(this).val()) == 0){
        $(this).select();
      }
    });
  })
}

function datePicker() {
  $(document).ready(function() {
    $('.datepicker').datepicker({ 
      format: 'd MM yyyy',
      autoclose: true
    });
  })
}

function formatDate(date) {
  var monthNames = [
  "January", "February", "March",
  "April", "May", "June", "July",
  "August", "September", "October",
  "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ', ' + year;
}

function closeModal(idModal){
  $('#'+idModal).modal("hide");
}

function postModalDetail(token, data, modalId, url){
  $.ajax({
    type: "POST",
    data: data,
    url: url,
    success: function (data) {
      $('#'+modalId).modal("show");
      $('#body_'+modalId).html(data);
    }
  });
}

function tooltip(){
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  });
}

function toast(msg, status){

  toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-bottom-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "9000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }

  if(status == 0){ toastr.error(msg); }

  if(status == 1){ toastr.success(msg); }

}

