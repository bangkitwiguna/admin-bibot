<?php 
 
return [
 
    /*
    |--------------------------------------------------------------------------
    | Home Page Language Lines
    |--------------------------------------------------------------------------
    |
    */
     
            /* LOGIN*/
            'log_judul' => 'Login',
            'log_subjudul' => 'Follow, copy & earn',
            'log_em' => 'Username',
            'log_forgot'=>'Input Username',
            'log_Pem' => 'Input your Username',
            'log_pass' => 'Password',
            'log_Ppass' => 'Input your password',
            'log_forgot' => 'Forgot Password',
            'log_submit' => 'Submit',
            'log_or' => 'or',
            'log_donot' => 'Do not have an account?',
            'log_create' => 'Create one',


            /* Register*/

          're_judul' => 'Register',
          're_subjudul' => 'Please fill out the information below to register as a new member',
          're_acc' => 'Account',
          're_ident' => 'Identity',
          're_doc' => 'Document',
          're_em' => 'Email',
          're_Pem' => 'Email',
          're_create' => 'Create Username',
          're_Pcreate' => 'Create Username',
          're_upline' => 'Upline Username',
          're_Pupline' => 'Upline Username',
          're_pass' => 'Password',
          're_Ppass' => 'Password',
          're_cpass' => 'Confirm Password',
          're_Pcpass' => 'Confirm Password',
          're_back' => 'Back',
          're_next' => 'Next',


          
               /* REGISTER2*/
               'r2_judul' => 'Register',
               'r2_subjudul' => 'Please fill out the information below to register as a new member',
               'r2_acc' => 'Account',
               'r2_ident' => 'Identity',
               'r2_title' => 'Title',
               'r2_referal' => 'Referal Code',
               'r2_doc' => 'Document',
               'r2_first' => 'Firstname',
               'r2_last' => 'Lastname',
               'r2_bdp' => 'Birth Place',
               'r2_bdd' => 'Birthday Date',
               'r2_Pbdd' => 'Select a Date',
               'r2_gender' => 'Gender',
               'r2_men' => 'Man',
               'r2_women' => 'Woman',
               'r2_phone' => 'Phone Number',
               'r2_idtype' => 'ID Card Type',
               'r2_idcard' => 'ID Card Number',
               'r2_country' => 'Country',
               'r2_postal' => 'Postal Code',
               'r2_add' => 'Address',
               'r2_next' => 'Next',


                /* REGISTER3*/
                'r3_judul' => 'Register',
                'r3_subjudul' => 'lorem ipsum dolor sit amet',
                'r3_acc' => 'Account',
                'r3_ident' => 'Identity',
                'r3_doc' => 'Document',
                'r3_upself' => 'Upload your selfie image',
                'r3_max25' => 'Max 25mb',
                'r3_front' => 'Front Side IC',
                'r3_back' => 'Back Side IC',
                'r3_proof' => 'Proof of resedential',
                'r3_selfie' => 'Selfie With IC',
                'r3_upload' => 'Upload',
                'r3_choose' => 'Choose file',
                'r3_agree' => 'I agree with the terms & conditions',
                'r3_finish' => 'Finish',
              


               /* Profile*/
               'prof_firstname'  => 'First Name',
               'prof_lastname'  => 'Last Name',
               'prof_info'  => 'Personal Information',
               'prof_nation'  => 'Nationality',
               'prof_idnumber'  => 'Identity Number',
               'prof_gender'  => 'Gender',
               'prof_pob'  => 'Place of Birth',
               'prof_dob'  => 'Date of Birth',


    
            /* nav*/
            'nav_profile' => 'Profile',
            'nav_nom' => 'Nomination',
            'nav_doc' => 'Documentaion',   
            'nav_change' => 'Change Password',
            'nav_login' => 'Login',
            'nav_regis' => 'Register',

               /* NOMINATION*/
               'nom_first' => 'FullName',
               'nom_Pfirst' => 'Input valid Fullname',
               'nom_last' => 'Last Name',
               'nom_Plast' => 'Input valid Last Name',
               'nom_phone' => 'Phone Number',
               'nom_Pphone' => 'Input valid Phone Number',
               'nom_relation' => 'Relationship',
               'nom_Prelation' => 'Input Relationship',
               'nom_email' => 'Email',
               'nom_Pemail' => 'Input valid Email',
               'nom_idtype' => 'ID Card Type',
               'nom_idnumber' => 'ID Card Number',
               'nom_iddoc' => 'Identity Document',
               'nom_Pidnumber' => 'Input valid ID Card Number',
               'nom_wills' => 'Wills',
               'nom_upload' => 'Upload',
               'nom_front' => 'Front Side IC',
               'nom_back' => 'Back Side IC',
               'nom_proof' => 'Proof of resedential',
               'nom_choose' => 'Choose file',
               'nom_add' => 'Address',
               'nom_pdf' => ' FILE_NAME_PDF',
               'nom_name' => 'Nominee Name',

           /* DOCUMENTAION*/
           'dok_type' => 'Document Type',
           'dok_com' => 'Comment',
           'dok_stat' => 'Status ',
           'dok_view' => 'View',
           'dok_upload' => 'Upload New Document',

           'dok_doc' => 'Upload New Document',
           'dok_idtype' => 'Identity Type',
           'dok_select' => 'Select Document',
           'dok_attachment' => 'Attachment',
           'dok_pdfmodal' => 'FILE_NAME_PDF',
           'dok_uploadmodal' => 'Upload',
           'dok_close' => 'Close',
           'dok_procced' => 'Proceed',
    
               /* DOCUMENTAION*/
      'cp_change' => 'Change Password',
      'cp_old' => 'Old Password',
      'cp_old2' => ' Input Old Password',
      'cp_newpass' => 'New Password',
      'cp_newpass2' => 'Input New Password',
      'cp_verif_pass' => 'Confirm New Password',
      'cp_verif_pass2' => 'Re-Type New Password',
      'cp_cancel' => 'Cancel',
      'cp_save' => 'Save',

        



    

         

   
];