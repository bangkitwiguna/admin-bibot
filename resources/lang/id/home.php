
<?php 
 
return [
 
    /*
    |--------------------------------------------------------------------------
    | Home Page Language Lines
    |--------------------------------------------------------------------------
    |
    */
   
      
           /* LOGIN*/
           'log_judul' => 'Login',
           'log_subjudul' => 'Ikuti, salin & hasilkan',
           'log_em' => 'Nama Pengguna',
           'log_forgot'=>'Masukan Nama Pengguna',
           'log_Pem' => 'Masukkan Nama Pengguna / Email Anda',
           'log_pass' => 'Kata Sandi',
           'log_Ppass' => 'Masukkan kata sandi Anda',
           'log_forgot' => 'Lupa kata sandi',
           'log_submit' => 'Submit',
           'log_or' => 'atau',
           'log_donot' => 'Belum punya akun?',
           'log_create' => 'Daftar',
      

               /* REGISTER*/
       're_judul' => 'Daftar',
       're_subjudul' => 'lorem ipsum dolor sit amet',
       're_acc' => 'Akun',
       're_ident' => 'Identitas',
       're_doc' => 'Dokumen',
       're_em' => 'E-mail',
       're_Pem' => 'E-mail',
       're_create' => 'Nama Pengguna',
       're_Pcreate' => 'Buat Nama Pengguna',
       're_upline' => 'Nama Depan',
       're_Pupline' => 'Buat Nama Depan',
       're_pass' => 'Kata Sandi',
       're_Ppass' => 'Kata Sandi',
       're_cpass' => 'Konfirmasi Kata Sandi',
       're_Pcpass' => 'Konfirmasi Kata Sandi',
       're_back' => 'Kembali',
       're_next' => 'Lanjut',


       
               /* REGISTER2*/
               'r2_judul' => 'Register',
               'r2_subjudul' => 'lorem ipsum dolor sit amet',
               'r2_acc' => 'Akun',
               'r2_title' => 'Gelar',
               'r2_referal' => 'Kode Rujukan',
               'r2_ident' => 'Identitas',
               'r2_doc' => 'Dokumen',
               'r2_first' => 'Nama Depan',
               'r2_last' => 'Nama Belakang',
               'r2_bdp' => 'Tempat Lahir',
               'r2_bdd' => 'Tanggal Lahir',
               'r2_Pbdd' => 'Pilih Tanggal Lahir',
               'r2_gender' => 'Jenis Kelamin',
               'r2_men' => 'Laki-laki',
               'r2_women' => 'Perempuan',
               'r2_phone' => 'NO Telephone',
               'r2_idtype' => 'Jenis ID Card ',
               'r2_idcard' => 'Nomor ID Card ',
               'r2_country' => 'Negara',
               'r2_postal' => 'Kode POS',
               'r2_add' => 'Alamat',
               'r2_next' => 'Next',


                  /* REGISTER3*/
                  'r3_judul' => 'Register',
                  'r3_subjudul' => 'lorem ipsum dolor sit amet',
                  'r3_acc' => 'Akun',
                  'r3_ident' => 'Identitas',
                  'r3_doc' => 'Dokumen',
                  'r3_upself' => 'Upload your selfie image',
                  'r3_max25' => 'Max 25mb',
                  'r3_front' => 'IC Sisi Depan',
                  'r3_back' => 'IC Sisi Belakang',
                  'r3_proof' => 'Bukti Tempat Tinggal',
                  'r3_upload' => 'Unggah',
                  'r3_choose' => 'Pilih FIle',
                  'r3_agree' => 'Saya setuju dengan syarat & ketentuan',
                  'r3_finish' => 'Selesai',
                

          /* Profile*/
          'prof_info' => 'Informasi Pribadi',
          'prof_nation'  => 'Kebangsaan',
          'prof_idnumber'  => 'Nomor Identitas',
          'prof_gender'  => 'Jenis Kelamin',
          'prof_pob'  => 'Tempat Tanggal Lahir',

           /* nav*/
           'nav_profile' => 'Profil',
           'nav_nom' => 'Nomination',
           'nav_dok' => 'Dokumentasi',
           'nav_change' => 'Ubah Password',
           'nav_login' => 'Masuk',
           'nav_reg' => 'Daftar',
      /* NOMINATION*/
      'nom_first' => 'Nama Lengkap',
      'nom_Pfirst' => 'Masukan Nama Lengkap',
      'nom_last' => 'Nama Belakang',
      'nom_Plast' => 'Masukan Nama Belakang',
      'nom_phone' => 'Nomor Telepon',
      'nom_Pphone' => 'Masukan Nomor Telephone ',
      'nom_mail' => 'E-mail',
      'nom_Pemail' => 'Masukan E-mail',
      'nom_idtype' => 'Kartu Identitas',
      'nom_idnumber' => 'Nomor Identitas',
      'nom_Pidnumber' => 'Masukan Nomor Kartu Identitas',
      'nom_wills' => 'Surat Wasiat',
      'nom_upload' => 'Unggah',
      'nom_front' => 'IC Sisi Depan',
      'nom_back' => 'IC Sisi Belakang',
      'nom_proof' => 'Bukti Tempat Tinggal',
      'nom_choose' => 'Pilih FIle',
      'nom_add' => 'Alamat',
      'nom_pdf' => ' NAMA_FILE_PDF',

      
        /* DOCUMENTAION*/
        'dok_type' => 'Tipe Dokumen',
        'dok_com' => 'Komentar',
        'dok_state' => 'Status ',
        'dok_view' => 'Lihat',
        'dok_upload' => 'Unggah Dokumen Baru',
  
        'dok_doc' => 'Unggah Dokumen Baru',
        'dok_idtype' => 'Tipe Identitas',
        'dok_select' => 'Pilih Dokumen',
        'dok_pdfmodal' => 'NAMA_FILE_PDF',
        'dok_uploadmodal' => 'Unggah',
        'dok_close' => 'Keluar',
        'dok_procced' => 'Proses',
  
  
        /* DOCUMENTAION*/
        'cp_change' => 'Ubah Password',
        'cp_old' => 'Password Lama',
        'cp_old2' => 'Masukan Password Lama',
        'cp_newpass' => 'Password Baru',
        'cp_newpass2' => 'Masukan Password Baru',
        'cp_verif_pass' => 'Konfirmasi Password Baru',
        'cp_verif_pass2' => 'Masukan Konfirmasi Password Baru',
        'cp_cancel' => 'Batal',
        'cp_save' => 'Simpan',

        




];