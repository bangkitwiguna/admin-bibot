<div class="container-fluid">
  <div class="row pb-3">
    <div class="col-md-6" style="display: inline-flex">
      <i class="fas fa-chevron-left fa-2x" style="cursor: pointer; margin-top: -2px" onclick="backHome()"></i>&nbsp;&nbsp;
      <h4>{!! $data->title !!} #{{$isi->kd_ticket}}</h4>
    </div>
  </div>

  <div class="mb-5 pb-5">
    @foreach ($isi->all as $item)
      @if($item->user != 'Mzone')
      <div class="row">
        <div class="col-md-8">
          <div class="card">
              <div class="card-body">
                <b>{{$item->user}}</b>
                <p>{{$item->description}}</p>
              </div>
          </div>
        </div>
        <div class="col-md-4">
        </div>
      </div>
      @else
      <div class="row">
        <div class="col-md-4">
        </div>
        <div class="col-md-8">
          <div class="card" style="background-color: #5AC3D9">
              <div class="card-body" style="color: white">
                <b>{{$item->user}}</b>
                <p>{{$item->description}}</p>
              </div>
          </div>
        </div>
      </div>
      @endif
    @endforeach
  </div>

  <footer class="main-footer">
    <form enctype="multipart/form-data" autocomplete="off" id="SimpanData" method="POST" onsubmit="return false;">
      <div class="form-group-inner">
        <div class="row">
          <div class="col-md-11">
            {!! Form::textarea('message', (isset($isi) ? '' : ''), [$disab, 'placeholder'=>'Leave Message Here', 'id'=>'message','class'=>'form-control', 'rows' => 3,]) !!}
            <span class="text-danger" id="message_error"></span>
          </div>
          <div class="col-md-1">          
            <input type="hidden" name="ke" value="{!! $ke !!}">
            <input type="hidden" name="kd_ticket" value="{!! $isi->kd_ticket !!}">
            {!! csrf_field() !!}
            @if($disab == '')
            <button style="width: 100%" class="btn btn-success" id="btn_simpan" type="button" onclick="simpan('SimpanData', 'btn_simpan', '{!! $data->url !!}')">Submit</button>@endif
          </div>
      </div>
    </form>

  </footer>
</div>