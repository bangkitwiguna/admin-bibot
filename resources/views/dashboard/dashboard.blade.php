@extends('layouts.layout')

@section('content')
  @if(session('super_admin'))

  <div class="row">
    @if(isset($dashboard2->saldoPazemo))
    @foreach ($dashboard2->saldoPazemo as $pzm)
    @if($pzm->currencyId == 'USDT')
    <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <b>Pazemo Balance (USDT)</b>
              <h2>{{number_format($pzm->balance, '2', ',', '.')}}</h2>
          </div>
      </div>
    </div>
    @endif
    @endforeach
    @endif
    <div class="col-md-6">
      <div class="card">
          <div class="card-body">
              <b>Binance Balance (USDT)</b>
              <h2>{{(isset($binance) ? number_format($binance->balance, '2', ',', '.') : '')}}</h2>
          </div>
      </div>
    </div>
  </div>
  <div class="row">
    @foreach ($binance->details as $bnc)
    <div class="col-md-4">
      <div class="card">
          <div class="card-body">
              <b>{{$bnc->status}}</b>
              <h2>{{$bnc->count}}</h2>
          </div>
      </div>
    </div>  
    @endforeach
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="card">
          <div class="card-body">
              <b>Total Wallet Member (USDT)</b>
              <h2>{{(isset($dashboard2) ? number_format($dashboard2->balance, '2', ',', '.') : '-')}}</h2>
          </div>
      </div>
    </div>
  </div>
  <div class="row">
    @foreach ($dashboard2->user_summary as $user)
    @if($user->status != 'verified' && $user->status != 'delete' && $user->status != null)
    <div class="col-md-3">
      <div class="card">
          <div class="card-body">
              <b>Users {{ucfirst($user->status)}}</b>
              <h2>{{$user->total}}</h2>
          </div>
      </div>
    </div>
    @endif
    @endforeach
  </div>
  @endif
</div>
@endsection


