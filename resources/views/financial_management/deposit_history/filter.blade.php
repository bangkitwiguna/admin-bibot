<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">
                      <label for="status" class="form-label">Status</label>
                      <select class="form-control" id="status" onchange="resetData()">
                          <option value="">All</option>
                          <option value="Processed">Processed</option>
                          <option value="Success">Success</option>
                          <option value="Rejected">Rejected</option>
                      </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="start_date" class="form-label">Start Date</label>
                        <input type="date" class="form-control" id="start_date" value="{!! date('Y-m-d', strtotime("-15 day")) !!}" onchange="resetData()">
                    </div>
                    <div class="col-sm-4">
                        <label for="end_date" class="form-label">End Date</label>
                        <input type="date" class="form-control" id="end_date" value="{!! date('Y-m-d') !!}" onchange="resetData()">
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
	select2();
	
    var token = "{!! csrf_token() !!}";
		var currentPage = 1;
		var totalPage = 0;
		var allData = 100;

		resetData();
		function resetData() {
			currentPage = 1;
			totalPage = 0;
			$("#myData").html("");
			getData()
		}
		
		function change(){
      currentPage = $("#select_pagination option:selected").val();
			getData()
		}

		function getData() {
      let pl = $("#pageLoaded option:selected").val();
      let start_date = $("#start_date").val();
      let end_date = $("#end_date").val();
      let status = $("#status").val();
			$.ajax({
				type: "POST",
				data: {_token :token, ke:'dataAll', pageLoaded: pl, currentPage: currentPage, start_date: start_date, end_date: end_date, status: status},
				url: '{!! $data->url !!}',
				success: function (data) {
					console.log(data);
					$('#myData').html(data.table)

					pagination(data.pagination)
				},
				error :function( data ) {
				console.log(data.responseText);

				var sts = 'Failed! Check error';
				toast(sts, 0);
				}
			});
		}

		function pagination(data){
			totalPage = data.lastPage ?? totalPage
			$('#max_pagination').html('Of&nbsp;&nbsp;'+totalPage)

			var paginate = '';
			for(a=1; a<=totalPage; a++){
				paginate = paginate+'<option>'+a+'</option>';
			}
			$('#select_pagination').html(paginate);

			$('#select_pagination').val(currentPage);
			select2();

			allData = data.total ?? allData
		}
		
    function exportExcel(){
      let pl = $("#pageLoaded option:selected").val();
      let start_date = $("#start_date").val();
      let end_date = $("#end_date").val();
      let status = $("#status").val();
      let bank_type = $("#bank_type").val();
      let username = $("#search").val();
      
      const url = '{{$data->url}}/exportExcel/limit='+pl+'&page='+currentPage+'&username='+username+'&startDate='+start_date+'&endDate='+end_date+'&status='+status+'&type='+bank_type;
      window.open(url, '_blank');
    };
</script>