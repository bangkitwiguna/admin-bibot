<div class="header-top-area">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="header-top-wraper">
          <div class="row">
            <div class="col-lg-1 col-md-1 col-sm-0 col-xs-0">
              <div class="menu-switcher-pro">
                <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                  <i class="educate-icon educate-nav">
                  </i>
                </button>
              </div>
            </div>                        
            <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
              <div class="header-right-info">
                <ul class="nav navbar-nav mai-top-nav header-right-menu">                                                      
                  @if(session('lang') != "id")
                  <li class="nav-item">
                    <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="false">
                      <span class="flag-icon flag-icon-us">
                      </span> 
                      <span class="admin-name"> English (US)
                      </span>
                      <i class="fa fa-angle-down edu-icon edu-down-arrow">
                      </i>
                    </a>
                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                      <li>
                        <a class="dropdown-item" href="{!! URL::to('/bahasa?lag=id') !!}">
                          <span class="flag-icon flag-icon-id">
                          </span> 
                          <span>Indonesia
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
                  @else
                  <li class="nav-item">
                    <a class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="false">
                      <span class="flag-icon flag-icon-id">
                      </span> 
                      <span class="admin-name">Indonesia
                      </span>
                      <i class="fa fa-angle-down edu-icon edu-down-arrow">
                      </i>
                    </a>
                    <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated zoomIn">
                      <li>
                        <a class="dropdown-item" href="{!! URL::to('/bahasa?lag=eng') !!}">
                          <span class="flag-icon flag-icon-us">
                          </span> 
                          <span>English (US)
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
                  @endif                                                      
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>