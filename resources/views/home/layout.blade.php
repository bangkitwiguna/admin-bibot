<?php 
$token = session('token'); //joikujikjpo,mo,o,jnj
$login = session('login'); //true
$fa = session('fa'); //true
?>

<!DOCTYPE html>
<html lang="en">

<head>
    @include('metahead')        
</head>

<body>
    <div class="container-fluid" style="background-color: white; height: auto; min-height: 100%; padding: 0">             
        @if (session("error"))                    
        <div class="alert alert-danger alert-mg-b alert-success-style4" style="margin-top: -30px">
            <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                    <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                </button>
            <i class="edu-danger-error admin-check-pro" aria-hidden="true"></i>
            <p><strong>{{session("error")}}</strong></p>
        </div>
        @endif
        @if (session("success")) 
        <div class="alert alert-success alert-success-style1" style="margin-top: -30px">
            <button type="button" class="close sucess-op" data-dismiss="alert" aria-label="Close">
                    <span class="icon-sc-cl" aria-hidden="true">&times;</span>
                </button>
            <i class="edu-checked-pro admin-check-pro" aria-hidden="true"></i>
            <p><strong>{{ session('success') }}</strong></p>
        </div>
        @endif
        @yield('content')        
    </div>

    <div id="ModalLoading" class="modal modal-edu-general fade" role="dialog">
        <div class="modal-dialog">            
            <center>
                <div class="preloader-single shadow-inner res-mg-b-30">
                    <div class="ts_preloading_box">
                        <div id="ts-preloader-absolute23">
                            <div class="tsperloader23" id="tsperloader23_one"></div>
                            <div class="tsperloader23" id="tsperloader23_two"></div>
                            <div class="tsperloader23" id="tsperloader23_three"></div>
                            <div class="tsperloader23" id="tsperloader23_four"></div>
                            <div class="tsperloader23" id="tsperloader23_five"></div>
                            <div class="tsperloader23" id="tsperloader23_six"></div>
                            <div class="tsperloader23" id="tsperloader23_seven"></div>
                            <div class="tsperloader23" id="tsperloader23_eight"></div>
                            <div class="tsperloader23" id="tsperloader23_big"></div>
                        </div>
                        <h4 style="color: #006DF0">Waiting ...</h4>
                    </div>
                </div>
            </center>
        </div>
    </div>
    @include('metascript')
    @stack('js')    
</body>
        
</html>
