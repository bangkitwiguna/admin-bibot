@extends('home.layout')
@section('content')
<div class="container" style="margin-top: 7em">        
  <div class="row aligncenter">
    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-0">
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" style="border-width: 1px; border-style: solid; padding: 30px; border-color: grey;">            
      <div class="text-center" style="margin-bottom: 2.5em">
        <h1>{{ __('home.log_judul') }} Admin</h1>
        <img src="{{ env('IMG') }}" alt="" width="100">
      </div>                            
      <form enctype="multipart/form-data" id="loginData" method="POST" onsubmit="return false;">
        <div class="form-group">
          <label for="" class="font-weight-bold">Email</label>
          <input id="username" type="text" name="username" class="form-control"
          placeholder="Input Email" required autofocus>
        </div>
        <div class="form-group">
          <label for="" class="font-weight-bold">{{ __('home.log_pass') }}</label>
          <input id="password" type="password" name="password" class="form-control"
          placeholder="{{ __('home.log_Ppass') }}" required autofocus>
          <input type="checkbox" style="margin-top: 10px" class="form-checkbox"> Show password
        </div>
        {{ csrf_field() }}
        <div class="form-group">
          <input type="hidden" name="ke" value="login" />
          <button onclick="login()" type="submit" class="btn btn-primary" id="submitBtn" style="width: 100%;">{{ __('home.log_submit') }}</button>
        </div>
        {{-- <div class="form-group" style="text-align: center">
          <a href="{{ URL::to('forgot-password') }}"><small>{{ __('home.log_forgot') }}</small></a>
        </div> --}}
      </form>
    </div>
  </div>
</div>

<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=6LcCWOgbAAAAACcdidte0EFykwF2LYkgf0AtrpgC" async defer></script>
@endsection

@push('js')
<script>
  $('.form-checkbox').click(function(){
    if($(this).is(':checked')){
      $('#password').attr('type','text');
    }else{
      $('#password').attr('type','password');
    }
  });

  function login(){
    try {
      $('#ModalLoading').modal("show");
      $.ajax({
        url: "{!! $data->url !!}",
        type: 'POST',
        data: new FormData($("#loginData")[0]),
        contentType: false,
        processData: false,      
        success: function (data) {
          $('#ModalLoading').modal("hide");
          console.log(data);
          if (data.rtn == 1) {
            Lobibox.notify('success', {
              size: 'mini',
              showClass: 'rollIn',
              hideClass: 'rollOut',
              msg: 'Login Success',
            });
            setTimeout(() => {                
              $('#ModalLoading').modal("hide");
              window.location.href = "{!! URL::to('/dashboard') !!}";
            }, 2000)
          } else {                    
            Lobibox.notify('error', {
              size: 'mini',
              showClass: 'rollIn',
              hideClass: 'rollOut',
              msg: data.msg,
            });
            setTimeout(() => {                
              $('#ModalLoading').modal("hide");
            }, 2000);
          }
        },
        error: function (data) {}
      });
    } catch(err){                
      Lobibox.notify('warning', {
        size: 'mini',
        showClass: 'rollIn',
        hideClass: 'rollOut',
        msg: err,
      });
    }     
  }
</script>
@endpush
