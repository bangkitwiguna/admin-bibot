@extends('layouts.layout')

@section('content')
<div id="table-form"></div>
<div id="input-form"></div>
<input type="hidden" id="token" value="{!! csrf_token() !!}">
<input type="hidden" id="input_data" value="0">
@endsection

@push('js')

<script type="text/javascript">
	function getForm() {
		$('#modalLoading').modal("show");
		$.ajax({
			type: "GET",
			url: "{!! $data->url !!}/getForm",
			success: function (data) {
				$('#input-form').html(data);
				$('#table-form').hide();
				$('#modalLoading').modal('hide');
			},
      error :function( data ) {
        var sts = 'Failed';
        toast(sts, 0);
        $('#modalLoading').modal('hide');
      }
		});
	}

	backHome();
	function backHome() {
		$('#modalLoading').modal("show");
		if ($("#input_data").val() == 0) {
			$.ajax({
				type: "GET",
				url: "{!! $data->url !!}/backHome",
				success: function (data) {
					$('#table-form').html(data);
					$('#modalLoading').modal('hide');
					$("#input_data").val('1');
				},
        error :function( data ) {
          var sts = 'Failed';
          toast(sts, 0);
          $('#modalLoading').modal('hide');
        }
			});
		} else {
			$('#input-form').html('');
			$('#table-form').show();
			$('#modalLoading').modal('hide');
		}
	}
	
	$(document).on('click', '.edit', function () {
		var dataCode = $(this).attr('data-code');
		var token = $('#token').val();
		$('#modalLoading').modal("show");

		$.ajax({
			type: "POST",
			data: {
				_token: token,
				ke: 'editData',
				dataCode: dataCode,
				disab: ''
			},
			url: "{!! $data->url !!}",
			success: function (data) {
				$('#input-form').html(data);
				$('#table-form').hide();
				$('#modalLoading').modal('hide');
			},
      error :function( data ) {
        var sts = 'Failed';
        toast(sts, 0);
        $('#modalLoading').modal('hide');
      }
		});
	});
	
  $(document).on('click', '.rankDetail', function () {
		var dataCode = $(this).attr('data-code');
		let robot = $("#robot option:selected").val();      
		var token = $('#token').val();
		$('#modalLoading').modal("show");

		$.ajax({
			type: "POST",
			data: {
				_token: token,
				ke: 'rankDetail',
				dataCode: dataCode,
        robot: robot,
				disab: ''
			},
			url: "{!! $data->url !!}",
			success: function (data) {
				$('#input-form').html(data);
				$('#table-form').hide();
				$('#modalLoading').modal('hide');
			},
      error :function( data ) {
        var sts = 'Failed';
        toast(sts, 0);
        $('#modalLoading').modal('hide');
      }
		});
	});


	$(document).on('click', '.detail', function () {
		var dataCode = $(this).attr('data-code');
		var token = $('#token').val();
		$('#modalLoading').modal("show");

		$.ajax({
			type: "POST",
			data: {
				_token: token,
				ke: 'editData',
				dataCode: dataCode,
				disab: 'disabled'
			},
			url: "{!! $data->url !!}",
			success: function (data) {
				$('#input-form').html(data);
				$('#table-form').hide();
				$('#modalLoading').modal('hide');
			},
      error :function( data ) {
        var sts = 'Failed';
        toast(sts, 0);
        $('#modalLoading').modal('hide');
      }
		});
	});


	$(document).on('click', '.delete', function () {
		var dataCode = $(this).attr('data-code');
		var token = $('#token').val();

		Swal.fire({
			title: 'Are You Sure?',
			text: 'Data cannot be restored!',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD6B55',
			confirmButtonText: 'Yes, Delete Data!'
		}).then((result) => {
			if (result.value) {
				$('#modalLoading').modal("show");
				$.ajax({
					type: "POST",
					data: {
						_token: token,
						ke: 'deleteData',
						dataCode: dataCode,
						disab: 'disabled'
					},
					url: "{!! $data->url !!}",
					success: function (data) {
						console.log(data);
						if (data.rtn == 1) {
							setTimeout(function () {
								var err =
									'<div id="alert-success" class="alert alert-success" role="alert">' +
									data.status + '</div>';
								$('#alert-home').html(err);
								hideSlow('alert-success');
                setTimeout(function(){ location.reload(true); }, 1000);
							}, 900);
						}
						if (data.rtn == 0) {
							var err =
								'<div id="alert-failed" class="alert alert-danger" role="alert">' +
								data.status + '</div>';
							$('#alert-home').html(err);
							hideSlow('alert-failed');
						}
						if (data.rtn == 3) {
							var err =
								'<div id="alert-failed" class="alert alert-danger" role="alert">' +
								data.status + '</div>';
							$('#alert-home').html(err);
							hideSlow('alert-success');
						}

            toast(data.status, data.rtn);
            $('#modalLoading').modal('hide'); 
					},
          error :function( data ) {
            var sts = 'Failed';
            toast(sts, 0);
            $('#modalLoading').modal('hide');
          }
				});
			}
		});
	});
</script>
@endpush