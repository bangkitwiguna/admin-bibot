<div class="container-fluid">
	<div class="row pb-3">
		<div class="col-md-6">
			<h4>{!! $data->title !!} @if(isset($data->sub_title)) &nbsp;&nbsp;&nbsp; / {!! $data->sub_title !!} @endif
			</h4>
		</div>
		<div class="col-md-6">
			<div class="m-right-align">
				@if(!isset($show_add))
				<button type="button" onclick="getForm()" class="btn btn-success btn-sm"
					style="float: right; margin-left: 10px;"><i class="fa fa-plus"></i> &nbsp;&nbsp; {!! $data->title
					!!}</button>
				@endif

				<div id="btn-excel"></div>

			</div>
		</div>
	</div>

	@if(isset($top_data))
	@include($top_data)
	@endif

	@if(isset($filter))
	@include($filter)
	@endif

	<div id="alert-home"></div>

	@if(isset($alert_warning))
	<div class="alert alert-warning" role="alert">{!! $alert_warning !!}</div>
	@endif

	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body p-0">

					<div class="row">
						<div class="col-sm-6 p-3 pt-2">
							<div class="form-inline">
								<label for="show">Show&nbsp;&nbsp;</label>
								<select class="form-control" id="pageLoaded" onchange="resetData()">
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select>
								<label for="show">&nbsp;&nbsp;Entries</label>
							</div>

						</div>
						<div class="col-sm-6">
						</div>
					</div>

					<div class="table-responsive">
						<table class="table table-hover" style="width: 100%;">
							<thead>
								<tr>
									@foreach($data->tableHeader as $tabHead)
									<th>{!! $tabHead !!}</th>
									@endforeach
								</tr>
							</thead>
							<tbody id="myData"></tbody>
						</table>
					</div>

					<div class="p-3">

						<div class="form-group row">
							<div class="col-sm-10"></div>
							<div class="col-sm-1">
								<select class="form-control select2" id="select_pagination" onchange="change()">
									<option>1</option>
								</select>
							</div>
							<label for="staticEmail" class="col-sm-1 col-form-label" id="max_pagination">Of 1</label>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>

@if(!isset($filter))
<script type="text/javascript">
	select2();
	
    var token = "{!! csrf_token() !!}";
		var currentPage = 1;
		var totalPage = 0;
		var allData = 100;

		resetData();
		function resetData() {
			currentPage = 1;
			totalPage = 0;
			$("#myData").html("");
			getData()
		}
		
		function change(){
      currentPage = $("#select_pagination option:selected").val();
			getData()
		}

		function getData() {
      let pl = $("#pageLoaded option:selected").val();
      $('#modalLoading').modal("show");
			$.ajax({
				type: "POST",
				data: {_token :token, ke:'dataAll', pageLoaded: pl, currentPage: currentPage },
				url: '{!! $data->url !!}',
				success: function (data) {
          $('#modalLoading').modal("hide");
					console.log(data);
					$('#myData').html(data.table)
					pagination(data.pagination)
				},
				error :function( data ) {
          $('#modalLoading').modal("hide");
          console.log(data.responseText);
          var sts = 'Failed! Check error';
          toast(sts, 0);
				}
			});
		}

		function pagination(data){
			totalPage = data.lastPage ?? totalPage
			$('#max_pagination').html('Of&nbsp;&nbsp;'+totalPage)

			var paginate = '';
			for(a=1; a<=totalPage; a++){
				paginate = paginate+'<option>'+a+'</option>';
			}
			$('#select_pagination').html(paginate);

			$('#select_pagination').val(currentPage);
			select2();

			allData = data.total ?? allData
		}
		
</script>
@endif