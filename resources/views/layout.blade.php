<?php 
$token = session('token');
$login = session('login');
$fa = session('fa');
?>

<!DOCTYPE html>
<html lang="en">

<head>
    @include('metahead')
</head>

<body>   
    @if(session('lang') != "id")
    @include('sidebar')
    @else
    @include('id.sidebar')
    @endif
    <div class="all-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button style="display: inline-block; position: absolute; margin-top: 6px" id="btn-nav" type="button" class="btn bar-button-pro header-drl-controller-btn btn-default navbar-btn">
                        <i class="educate-icon educate-nav"></i>
                    </button>

                    <div class="logo-pro">                                                
                        <a href="{!! URL::to('/') !!}"><img class="main-logo" width="175px" src="{!! URL::asset('img/logo.png')!!}" alt="" /></a>
                    </div>                    
                </div>
            </div>
        </div>
        <div class="header-advance-area">
            @include('header')
        </div>
        @yield('content')
    </div>
    
    <div id="ModalLoading" class="modal modal-edu-general fade" role="dialog">
        <br><br><br><br>
        <div class="modal-dialog">                        
            <center>                
                <div class="preloader-single shadow-inner res-mg-b-30">
                    <div class="ts_preloading_box">
                        <div id="ts-preloader-absolute23">
                            <div class="tsperloader23" id="tsperloader23_one"></div>
                            <div class="tsperloader23" id="tsperloader23_two"></div>
                            <div class="tsperloader23" id="tsperloader23_three"></div>
                            <div class="tsperloader23" id="tsperloader23_four"></div>
                            <div class="tsperloader23" id="tsperloader23_five"></div>
                            <div class="tsperloader23" id="tsperloader23_six"></div>
                            <div class="tsperloader23" id="tsperloader23_seven"></div>
                            <div class="tsperloader23" id="tsperloader23_eight"></div>
                            <div class="tsperloader23" id="tsperloader23_big"></div>
                        </div>
                    </div>
                    <h4 style="color: #006DF0">Please Wait ...</h4>
                </div>
            </center>
        </div>
    </div>
    @include('metascript')
    <script type="text/javascript">    
      $('.owl-carousel').owlCarousel({
          loop:true,
          margin:10,
          responsive:{
              0:{
                  items:1
              },
              600:{
                  items:1
              },
              1000:{
                  items:1
              }
          }
      })
    </script>
    @stack('js')    
    @stack('js2')
</body>

</html>