<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-dark" style="background-color:#423E92; border-color:#423E92;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars  text-white"></i></a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link  text-white">Home</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link  text-white">Contact</a>
        </li> -->
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        
        
        <li class="nav-item dropdown">
            <a class="nav-link text-white" data-toggle="dropdown" href="#">
                Hello, &nbsp;
                {{session('name')}}
            </a>
            {{-- <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <a href="{ { route('logout') } }"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <div class="media-body">
                            Logout
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                
            </div> --}}
        </li>

    </ul>
</nav>
<!-- /.navbar -->


{{-- 
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form> --}}