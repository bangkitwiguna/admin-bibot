<!-- jQuery -->
<script src="{!! URL::asset('plugins/jquery/jquery.min.js') !!}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{!! URL::asset('plugins/jquery-ui/jquery-ui.min.js') !!}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{!! URL::asset('plugins/bootstrap/js/bootstrap.bundle.min.js') !!}"></script>

<!-- Select2 -->
<script src="{!! URL::asset('plugins/select2/js/select2.full.min.js') !!}"></script>
<!-- DataTables  & Plugins -->
<script src="{!! URL::asset('plugins/datatables/jquery.dataTables.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-responsive/js/dataTables.responsive.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-responsive/js/responsive.bootstrap4.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-buttons/js/dataTables.buttons.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-buttons/js/buttons.bootstrap4.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/jszip/jszip.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/pdfmake/pdfmake.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/pdfmake/vfs_fonts.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-buttons/js/buttons.html5.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-buttons/js/buttons.print.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/datatables-buttons/js/buttons.colVis.min.js') !!}"></script>

<!-- ChartJS -->
<script src="{!! URL::asset('plugins/chart.js/Chart.min.js') !!}"></script>
<!-- Sparkline -->
<script src="{!! URL::asset('plugins/sparklines/sparkline.js') !!}"></script>
<!-- JQVMap -->
<script src="{!! URL::asset('plugins/jqvmap/jquery.vmap.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/jqvmap/maps/jquery.vmap.usa.js') !!}"></script>
<!-- jQuery Knob Chart -->
<script src="{!! URL::asset('plugins/jquery-knob/jquery.knob.min.js') !!}"></script>
<!-- daterangepicker -->
<script src="{!! URL::asset('plugins/moment/moment.min.js') !!}"></script>
<script src="{!! URL::asset('plugins/daterangepicker/daterangepicker.js') !!}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{!! URL::asset('plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') !!}"></script>
<!-- Summernote -->
<script src="{!! URL::asset('plugins/summernote/summernote-bs4.min.js') !!}"></script>
<!-- overlayScrollbars -->
<script src="{!! URL::asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') !!}"></script>
<!-- SweetAlert2 -->
<script src="{!! URL::asset('plugins/sweetalert2/sweetalert2.min.js') !!}"></script>
<!-- Toastr -->
<script src="{!! URL::asset('plugins/toastr/toastr.min.js') !!}"></script>
<!-- AdminLTE App -->
<script src="{!! URL::asset('dist/js/adminlte.js') !!}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{!! URL::asset('dist/js/demo.js') !!}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{!! URL::asset('dist/js/pages/dashboard.js') !!}"></script>
<script src="{!! URL::asset('plugins/custom-add.js') !!}"></script>
<script src="https://unpkg.com/ionicons@5.5.2/dist/ionicons.js"></script>
<!-- generate tree JS ----->
<script src="{!! URL::asset('plugins/tree/generate-tree.js')!!}"></script>
