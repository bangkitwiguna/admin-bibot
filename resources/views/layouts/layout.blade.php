<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{env('NAME')}}</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{env('IMG')}}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    @include('layouts.css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body class="sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed" style="height: auto;">
    <div class="wrapper">
        @include('layouts.header')
        @include('layouts.sidebar')

        <!-- Page wrapper  -->
        <div class="content-wrapper pl-4 pr-4" style="min-height: 213.016px;">
            <br />
            @yield('content')
        </div>

        <div class="modal" id="modalLoading" data-backdrop="static">
          <div class="modal-dialog" role="document"  style="color: #fff;">
              <center>
                  <i class="fas fa-spinner fa-pulse fa-3x"></i><br/><br/>
                  Waiting...
              </center>
          </div>
        </div>
        <!-- End Page wrapper  -->
        @include('layouts.js')

        @stack('js')
</body>

</html>