<?php 
$seg1 = Request::segment(1) ?? '';
$seg2 = Request::segment(2) ?? '';
?>

<aside class="main-sidebar sidebar-light-reval elevation-1">
    <!-- Brand Logo -->
    <a href="{!! URL::to('/') !!}" class="brand-link p-1" style="text-align: center">
        <span class="brand-text font-weight-light m-4"><img src="{{ env('IMG') }}" style="height:48px;"/></span>
    </a>

    <div class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-transition os-host-scrollbar-horizontal-hidden"><div class="os-resize-observer-host observed"><div class="os-resize-observer" style="left: 0px; right: auto;"></div></div><div class="os-size-auto-observer observed" style="height: calc(100% + 1px); float: left;"><div class="os-resize-observer"></div></div><div class="os-content-glue" style="margin: 0px -8px; width: 249px; height: 165px;"></div><div class="os-padding"><div class="os-viewport os-viewport-native-scrollbars-invisible" style="overflow-y: scroll;"><div class="os-content" style="padding: 0px 8px; height: 100%; width: 100%;">

    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-collapse-hide-child nav-child-indent nav-compact nav-legacy"
            data-widget="treeview" role="menu" data-accordion="false">
            {{-- <li class="nav-item @if($seg1 == '') menu-open @endif">
                <a href="{!! URL::to('/') !!}" class="nav-link">
                    <i class="nav-icon fas" style="vertical-align: sub;"><ion-icon name="grid-outline" style="font-size:15pt;"></ion-icon></i>
                    
                    <p>
                        Dashboard
                    </p>
                </a>
            </li> --}}

            <li class="nav-item @if($seg1 == 'member') menu-open @endif">
              <a href="#" class="nav-link  @if($seg1 == 'member') active @endif">
                  <i class="nav-icon fas" style="vertical-align: sub;"><ion-icon name="calculator-outline" style="font-size:15pt;"></ion-icon></i>
                  <p>
                      Member Area
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview" @if($seg1 != 'member') style="display: none;" @endif>
                  <li class="nav-item"><a href="{!! URL::to('/member/master-member') !!}" class="nav-link @if($seg2 == 'master-member') active @endif"> <i class="far fa-circles nav-icon"></i> <p>Member Master</p></a>
                  </li>
              </ul>
            </li>

            <li class="nav-item @if($seg1 == 'trader') menu-open @endif">
              <a href="#" class="nav-link  @if($seg1 == 'trader') active @endif">
                  <i class="nav-icon fas" style="vertical-align: sub;"><ion-icon name="people" style="font-size:15pt;"></ion-icon></i>
                  <p>
                      Trader Area
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview" @if($seg1 != 'trader') style="display: none;" @endif>
                  <li class="nav-item"><a href="{!! URL::to('/trader/master-trader') !!}" class="nav-link @if($seg2 == 'master-trader') active @endif"> <i class="far fa-circles nav-icon"></i> <p>Trader Master</p></a>
                  </li>
              </ul>
            </li>
            
            <li class="nav-item @if($seg1 == 'finance') menu-open @endif">
              <a href="#" class="nav-link  @if($seg1 == 'finance') active @endif">
                  <i class="nav-icon fas" style="vertical-align: sub;"><ion-icon name="file-tray-full-outline" style="font-size:15pt;"></ion-icon></i>
                  <p>
                      Finance
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview" @if($seg1 != 'withdraw') style="display: none;" @endif>
                  <li class="nav-item"><a href="{!! URL::to('/finance/withdraw') !!}" class="nav-link @if($seg2 == 'withdraw') active @endif"> <i class="far fa-circles nav-icon"></i> <p>Withdraw Data</p></a>
                  </li>
              </ul>
              <ul class="nav nav-treeview" @if($seg1 != 'deposit') style="display: none;" @endif>
                  <li class="nav-item"><a href="{!! URL::to('/finance/deposit') !!}" class="nav-link @if($seg2 == 'deposit') active @endif"> <i class="far fa-circles nav-icon"></i> <p>Deposit Data</p></a>
                  </li>
              </ul>
            </li>
            
            <li class="nav-item @if($seg1 == 'cs') menu-open @endif">
              <a href="#" class="nav-link  @if($seg1 == 'cs') active @endif">
                  <i class="nav-icon fas" style="vertical-align: sub;"><ion-icon name="ticket-outline" style="font-size:15pt;"></ion-icon></i>
                  <p>
                      Customer Care
                      <i class="right fas fa-angle-left"></i>
                  </p>
              </a>
              <ul class="nav nav-treeview" @if($seg1 != 'ticket') style="display: none;" @endif>
                  <li class="nav-item"><a href="{!! URL::to('/cs/ticket') !!}" class="nav-link @if($seg2 == 'ticket') active @endif"> <i class="far fa-circles nav-icon"></i> <p>Ticket</p></a>
                  </li>
              </ul>
            </li>

            <li class="nav-item @if($seg1 == '') menu-open @endif">
                <a href="{!! URL::to('/logout') !!}" class="nav-link">
                    <i class="nav-icon fas" style="vertical-align: sub;"><ion-icon name="log-out-outline" style="font-size:15pt;"></ion-icon></i>                    
                    <p>
                        Logout
                    </p>
                </a>
            </li>

        </ul>
    </nav>
    <!-- /.sidebar-menu -->
    <!-- /.sidebar -->
    </div></div></div><div class="os-scrollbar os-scrollbar-horizontal os-scrollbar-unusable os-scrollbar-auto-hidden"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="width: 100%; transform: translate(0px, 0px);"></div></div></div><div class="os-scrollbar os-scrollbar-vertical os-scrollbar-auto-hidden"><div class="os-scrollbar-track"><div class="os-scrollbar-handle" style="height: 19.0689%; transform: translate(0px, 79.2655px);"></div></div></div><div class="os-scrollbar-corner"></div></div>
</aside>