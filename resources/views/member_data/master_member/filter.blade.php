<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">
                      <label for="status" class="form-label">Status</label>
                      <select class="form-control" id="status" onchange="resetData()">
                          <option value="">All</option>
                          <option value="1">Active</option>
                          <option value="0">Not Active</option>
                      </select>
                    </div>
                    {{-- <div class="col-sm-2">
                      <label for="rank" class="form-label">Rank</label>
                      <select class="form-control" id="rank" onchange="resetData()">
                        
                      </select>
                    </div> --}}
                    <div class="col-sm-10">
                        <label for="search" class="form-label">Search Username</label>
                        <div style="display: flex">
                          <input type="text" class="form-control" id="search" placeholder="Search Username...">
                          <button style="margin-left: 10px" class="btn btn-success" onclick="resetData()">Search</button>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">	
  select2();
  
  var token = "{!! csrf_token() !!}";
  var currentPage = 1;
  var totalPage = 0;
  var allData = 100;

  resetData();
  function resetData() {
    currentPage = 1;
    totalPage = 0;
    $("#myData").html("");
    getData()
  }
  
  function change(){
    currentPage = $("#select_pagination option:selected").val();
    getData()
  }

  function getData() {
    let username = $('#search').val();
    let pl = $("#pageLoaded option:selected").val();
    let status = $("#status option:selected").val();      
    $('#modalLoading').modal('show');
    $.ajax({
      type: "POST",
      data: {
        _token :token, 
        ke:'dataAll', 
        pageLoaded: pl, 
        currentPage: currentPage, 
        status: status, 
        username: username, 
      },
      url: '{!! $data->url !!}',
      success: function (data) {
        $('#modalLoading').modal('hide');
        console.log(data);
        $('#myData').html(data.table)
        pagination(data.pagination)
      },
      error :function( data ) {
        $('#modalLoading').modal('hide');
        console.log(data.responseText);
        var sts = 'Failed! Check error';
        toast(sts, 0);
      }
    });
  }

  function pagination(data){
    totalPage = data.lastPage ?? totalPage
    $('#max_pagination').html('Of&nbsp;&nbsp;'+totalPage)
    var paginate = '';
    for(a=1; a<=totalPage; a++){
      paginate = paginate+'<option>'+a+'</option>';
    }
    $('#select_pagination').html(paginate);
    $('#select_pagination').val(currentPage);
    select2();
    allData = data.total ?? allData
  }
</script>