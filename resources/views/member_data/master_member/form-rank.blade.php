<div class="container-fluid">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-hover" style="width: 100%;">
							<thead>
                <th>No</th>
                <th>Username</th>
                <th>Full Name</th>
                <th>Email</th>
                <th>Country</th>
                <th>Referral</th>
                <th>Rank</th>
                <th>Status</th>
                <th>Teams</th>
                <th>Registered At</th>
							</thead>
							<tbody id="myData">
                @if(count($table)>0)
                  @php
                  $i=1;   
                  $total=0; 
                  $status = ['Non Active', 'Active'];
                  @endphp
                  @foreach($table as $tbl)
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{$tbl->username}}</td>
                    <td>{{$tbl->name}}</td>
                    <td>{{$tbl->email}}</td>
                    <td>{{$tbl->country}}</td>
                    <td>{{$tbl->referral}}</td>
                    <td>{{$tbl->rank}}</td>
                    <td>{{$status[$tbl->status]}}</td>
                    <td>{{$tbl->teams}}</td>
                    <td>{{$tbl->created_at}}</td>
                  </tr>
                  @php
                  $i++;
                  @endphp
                  @endforeach
                @else
                <tr>
                  <td colspan="10" style="text-align: center">The Data is Empty</td>
                </tr>
                @endif
              </tbody>
						</table>
            <div class="float-right">
              <button type="button" class="btn btn-outline-secondary" onclick="backHome()">Back</button>
            </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>