<div class="container-fluid">
  <div class="row pb-3">
    <div class="col-md-6">
      <h4>{!! $data->title !!}</h4>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <form enctype="multipart/form-data" autocomplete="off" id="SimpanData" method="POST" onsubmit="return false;">
        <div class="form-group-inner">
          <h4 style="text-align: center">Personal Information</h4><br>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group row">
                {!! Form::label('username', 'Username', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('username', (isset($isi) ? $isi->username : ''), [$disab, 'placeholder'=>'Username', 'id'=>'username','class'=>'form-control']) !!}
                  <span class="text-danger" id="username_error"></span>
                </div>
              </div>
              
              <div class="form-group row">
                {!! Form::label('full_name', 'Full Name', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('full_name', (isset($isi) ? $isi->name : ''), [$disab, 'placeholder'=>'Full Name', 'id'=>'full_name','class'=>'form-control']) !!}
                  <span class="text-danger" id="full_name_error"></span>
                </div>
              </div>
              
              <div class="form-group row">
                {!! Form::label('rd', 'Registered Date', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('rd', (isset($isi) ? date("m-d-Y", strtotime($isi->created_at)) : ''), [$disab, 'placeholder'=>'Registered Date', 'id'=>'rd','class'=>'form-control']) !!}
                  <span class="text-danger" id="rd_error"></span>
                </div>
              </div>            
              
              <div class="form-group row">
                {!! Form::label('member_rank', 'Rank', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('member_rank', (isset($isi) ? $isi->rank : ''), [$disab, 'placeholder'=>'Rank', 'id'=>'member_rank','class'=>'form-control']) !!}
                  <span class="text-danger" id="member_rank_error"></span>
                </div>
              </div>
            </div>

            <div class="col-sm-6">        
              <div class="form-group row">
                {!! Form::label('referal', 'Referral', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('referal', (isset($isi) ? $isi->referall : ''), [$disab, 'placeholder'=>'Referral', 'id'=>'referal','class'=>'form-control']) !!}
                  <span class="text-danger" id="referal_error"></span>
                </div>
              </div>

              <div class="form-group row">
                {!! Form::label('email', 'Email', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('email', (isset($isi) ? $isi->email : ''), [$disab, 'placeholder'=>'Email', 'id'=>'email','class'=>'form-control']) !!}
                  <span class="text-danger" id="email_error"></span>
                </div>
              </div>
              
              <div class="form-group row">
                {!! Form::label('status', 'Status', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  @php
                  $status = ['Non Active', 'Active'];    
                  @endphp
                  {!! Form::text('status', (isset($isi) ? $status[$isi->status] : ''), [$disab, 'placeholder'=>'Status', 'id'=>'status','class'=>'form-control']) !!}
                  <span class="text-danger" id="status_error"></span>
                </div>
              </div>
              
              <div class="form-group row">
                {!! Form::label('balance', 'Balance', ['class'=>'col-sm-3 col-form-label']) !!}
                <div class="col-sm-8">
                  {!! Form::text('balance', (isset($wallet) ? '$ '.number_format((float)$wallet->wallet_amount,2,'.','') : ''), [$disab, 'placeholder'=>'Balance', 'id'=>'balance','class'=>'form-control']) !!}
                  <span class="text-danger" id="balance_error"></span>
                </div>
              </div>

            </div>
          </div>

        <br />
        <div class="float-right">
          <input type="hidden" name="ke" value="{!! $ke !!}">
          {!! csrf_field() !!}
          <input type="hidden" id="level" value="{!! (isset($isi) ? $isi->level : '') !!}">
          <input type="hidden" name="id" value="{!! (isset($isi) ? $isi->id : '') !!}">
          <button type="button" class="btn btn-outline-secondary" onclick="backHome()">Back</button>
          @if($disab == '')<button class="btn btn-success" id="btn_simpan" type="button"
            onclick="simpan('SimpanData', 'btn_simpan', '{!! $data->url !!}')">Save</button>@endif
        </div>

      </form>
    </div>
  </div>
  <div id="alert-home"></div>
</div>

<script>  
</script>