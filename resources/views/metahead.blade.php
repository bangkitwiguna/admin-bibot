<?php 
$whitelist = array(
    '127.0.0.1',
    '::1'
);
$pb = '';
?>

<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<title>{{ env('NAME') }}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<!-- favicon
    ============================================ -->
<link rel="shortcut icon" type="image/x-icon" href="{{ env('IMG') }}">
<!-- Google Fonts
    ============================================ -->
<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
<!-- Bootstrap CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/bootstrap.min.css')!!}">
<!-- Bootstrap CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/font-awesome.min.css')!!}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">    
<!-- owl.carousel CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/owl.carousel.css')!!}">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/owl.theme.css')!!}">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/owl.transitions.css')!!}">
<!-- animate CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/animate.css')!!}">
<!-- normalize CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/normalize.css')!!}">
<!-- meanmenu icon CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/meanmenu.min.css')!!}">
<!-- datapicker CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/datapicker/datepicker3.css')!!}">
<!-- flag CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css') !!}" type="text/css">
<!-- main CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/main.css')!!}">
<!-- educate icon CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/educate-custon-icon.css')!!}">
<!-- morrisjs CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/morrisjs/morris.css')!!}">
<!-- mCustomScrollbar CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/scrollbar/jquery.mCustomScrollbar.min.css')!!}">
<!-- metisMenu CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/metisMenu/metisMenu.min.css')!!}">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/metisMenu/metisMenu-vertical.css')!!}">
<!-- calendar CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/calendar/fullcalendar.min.css')!!}">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/calendar/fullcalendar.print.min.css')!!}">
<!-- Modals CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/modals.css')!!}">
<!-- buttons CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/buttons.css')!!}">
<!-- alert CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/alerts.css')!!}">
<!-- forms CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/form/all-type-forms.css')!!}">
<!-- notifications CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/notifications/Lobibox.min.css')!!}">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/notifications/notifications.css')!!}">
<!-- select2
    ============================================ -->
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<!-- Preloader CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/preloader/preloader-style.css')!!}">
<!-- style CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/style.css')!!}">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/custom.css')!!}">
<!-- responsive CSS
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/responsive.css?version=1')!!}">
<!-- On/Off Auth
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/lc_switch.css') !!}" type="text/css">
<!-- iCheck Style
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.3/skins/square/blue.min.css') !!}" type="text/css">
<!-- Owl Carousel
    ============================================ -->
<link rel="stylesheet" href="{!! URL::asset($pb.'css/owl-carousel/owl.carousel.css') !!}" type="text/css">
<link rel="stylesheet" href="{!! URL::asset($pb.'css/owl-carousel/owl.theme.default.css') !!}" type="text/css">
<!-- modernizr JS
    ============================================ -->
<script src="{!! URL::asset('js/vendor/modernizr-2.8.3.min.js')!!}"></script>

