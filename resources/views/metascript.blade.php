<!-- jquery
============================================ -->
<script src="{!! URL::asset('js/vendor/jquery-1.12.4.min.js')!!}"></script>
<!-- bootstrap JS
    ============================================ -->
<script src="{!! URL::asset('js/bootstrap.min.js')!!}"></script>
<!-- wow JS
    ============================================ -->
<script src="{!! URL::asset('js/wow.min.js')!!}"></script>
<!-- price-slider JS
    ============================================ -->
<script src="{!! URL::asset('js/jquery-price-slider.js')!!}"></script>
<!-- meanmenu JS
    ============================================ -->
<script src="{!! URL::asset('js/jquery.meanmenu.js')!!}"></script>
<!-- select2
    ============================================ -->
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- owl.carousel JS
    ============================================ -->
<script src="{!! URL::asset('js/owl.carousel.min.js')!!}"></script>
<!-- sticky JS
    ============================================ -->
<script src="{!! URL::asset('js/jquery.sticky.js')!!}"></script>
<!-- scrollUp JS
    ============================================ -->
<script src="{!! URL::asset('js/jquery.scrollUp.min.js')!!}"></script>
<!-- datapicker JS
    ============================================ -->
<script src="{!! URL::asset('js/datapicker/bootstrap-datepicker.js')!!}"></script>
<script src="{!! URL::asset('js/datapicker/datepicker-active.js')!!}"></script>
<!-- counterup JS
    ============================================ -->
<script src="{!! URL::asset('js/counterup/jquery.counterup.min.js')!!}"></script>
<script src="{!! URL::asset('js/counterup/waypoints.min.js')!!}"></script>
<script src="{!! URL::asset('js/counterup/counterup-active.js')!!}"></script>
<!-- mCustomScrollbar JS
    ============================================ -->
<script src="{!! URL::asset('js/scrollbar/jquery.mCustomScrollbar.concat.min.js')!!}"></script>
<script src="{!! URL::asset('js/scrollbar/mCustomScrollbar-active.js')!!}"></script>
<!-- metisMenu JS
    ============================================ -->
<script src="{!! URL::asset('js/metisMenu/metisMenu.min.js')!!}"></script>
<script src="{!! URL::asset('js/metisMenu/metisMenu-active.js')!!}"></script>
<!-- morrisjs JS
    ============================================ -->
<script src="{!! URL::asset('js/morrisjs/raphael-min.js')!!}"></script>
<script src="{!! URL::asset('js/morrisjs/morris.js')!!}"></script>
<script src="{!! URL::asset('js/morrisjs/home3-active.js')!!}"></script>
<!-- morrisjs JS
    ============================================ -->
<script src="{!! URL::asset('js/sparkline/jquery.sparkline.min.js')!!}"></script>
<script src="{!! URL::asset('js/sparkline/jquery.charts-sparkline.js')!!}"></script>
<script src="{!! URL::asset('js/sparkline/sparkline-active.js')!!}"></script>
<!-- calendar JS
    ============================================ -->
<script src="{!! URL::asset('js/calendar/moment.min.js')!!}"></script>
<script src="{!! URL::asset('js/calendar/fullcalendar.min.js')!!}"></script>
<script src="{!! URL::asset('js/calendar/fullcalendar-active.js')!!}"></script>
<!-- plugins JS
    ============================================ -->
<script src="{!! URL::asset('js/plugins.js')!!}"></script>
<!-- generate tree JS
    ============================================ -->
<script src="{!! URL::asset('js/generate-tree.js')!!}"></script>
<!-- main JS
    ============================================ -->
<script src="{!! URL::asset('js/main.js')!!}"></script>
<!-- notification JS
    ============================================ -->
<script src="{!! URL::asset('js/notifications/Lobibox.js')!!}"></script>
<script src="{!! URL::asset('js/notifications/notification-active.js')!!}"></script>
<!-- tab JS
    ============================================ -->
<script src="{!! URL::asset('js/tab.js')!!}"></script>
<!-- icheck JS
    ============================================ -->
<script src="{!! URL::asset('https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.3/icheck.min.js')!!}"></script>
<!-- On/Off Auth
    ============================================ -->
<script src="{!! URL::asset('js/lc_switch.js') !!}"></script>
<!-- pwstrength JS
    ============================================ -->
<script src="{!! URL::asset('js/password-meter/pwstrength-bootstrap.min.js')!!}"></script>
<script src="{!! URL::asset('js/password-meter/zxcvbn.js')!!}"></script>
<script src="{!! URL::asset('js/password-meter/password-meter-active.js')!!}"></script>
<!-- Swal JS
    ============================================ -->
<script src="{!! URL::asset('https://unpkg.com/sweetalert/dist/sweetalert.min.js')!!}"></script>
<!-- sorttable JS
    ============================================ -->
<script src="{!! URL::asset('https://www.kryogenix.org/code/browser/sorttable/sorttable.js')!!}"></script>
<!-- Owl Carousel JS
    ============================================ -->
<script src="{!! URL::asset('js/owl-carousel/owl.carousel.js')!!}"></script>

<script>
$(window).load(function() {
    $("#btn-nav").on("click tap", function() {
      $(".left-sidebar-pro").toggleClass("showNav hideNav");
    });
    
    $("#nav-btn").on("click tap", function() {
      $(".left-sidebar-pro").toggleClass("showNav hideNav");
    });
});
</script>