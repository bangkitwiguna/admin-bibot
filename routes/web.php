<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/member/master-member');
});

Route::get('/logout', function () {
    session()->forget(['token']);
    return redirect('dashboard');
});

// Auth
Route::resource('/login', 'LoginController');

// Dashboard
Route::resource('/dashboard','DashboardController');
Route::get('/bahasa', 'DashboardController@bahasa');

// Member Data
Route::resource('/member/master-member','MemberData\MemberController');

// Trader Data
Route::resource('/trader/master-trader','TraderData\TraderController');

// Finance Data
Route::resource('/finance/withdraw','Finance\WithdrawController');
Route::resource('/finance/deposit','Finance\DepositController');

// Addsense Data
Route::resource('/adds/announcement','Addsense\AnnouncementController');

//Customer Care
Route::resource('/cs/ticket','CustomerCare\TicketController');